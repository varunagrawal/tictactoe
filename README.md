TicTacToe
=========

TicTacToe Playing Bot.

These are bots that you can play Tic Tac Toe with. 
Currently, they use a search based strategy to find the optimum move to play which renders them unbeatable. 

Will develop an easier Game-Theory based program soon.
